package com.axamit.core.filter.verification;

public abstract class BaseSlingVerifier<T> implements SlingVerifier<T> {
    private SlingVerifier<T> next;

    @Override
    public SlingVerifier<T> setNext(SlingVerifier<T> slingVerifier) {
        this.next = slingVerifier;
        return slingVerifier;
    }

    boolean verifyNext(T request) {
        if (next == null) {
            return true;
        }
        return next.isVerified(request);
    }
}