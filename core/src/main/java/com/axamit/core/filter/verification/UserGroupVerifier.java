package com.axamit.core.filter.verification;

import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Iterator;
import java.util.Optional;
import java.util.stream.StreamSupport;

public class UserGroupVerifier extends BaseSlingVerifier<SlingHttpServletRequest> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupVerifier.class);
    private String groupName;
    private String userId;
    private Boolean correctUserGroup = null;

    public UserGroupVerifier(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public boolean isVerified(SlingHttpServletRequest slingRequest) {
        ResourceResolver resourceResolver = slingRequest.getResourceResolver();
        Session session;
        UserManager manager;
        try {
            session = Optional.ofNullable(resourceResolver.adaptTo(Session.class))
                    .orElseThrow(() -> new RepositoryException("Unable to create repository session"));

            manager = Optional.ofNullable(resourceResolver.adaptTo(UserManager.class))
                    .orElseThrow(() -> new RepositoryException("Unable to create repository user manager"));

            if (!isUserGroupCorrect(manager, session)) {
                return false;
            }
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            return false;
        }
        userId = session.getUserID();
        return verifyNext(slingRequest);
    }

    private boolean isUserGroupCorrect(UserManager userManager, Session session) throws RepositoryException {
        Iterator<Group> groupIterator = userManager.getAuthorizable(session.getUserID()).memberOf();

        Iterable<Group> groupIterable = () -> groupIterator;
        return StreamSupport.stream(groupIterable.spliterator(), false).map(this::getGroupId).anyMatch(groupName::equals);
    }

    private String getGroupId(Group group) {
        String id;
        try {
            id = group.getID();
        } catch (RepositoryException e) {
            LOGGER.error(e.getMessage());
            id = null;
        }
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public Boolean isUserGroupCorrect() {
        return correctUserGroup;
    }
}