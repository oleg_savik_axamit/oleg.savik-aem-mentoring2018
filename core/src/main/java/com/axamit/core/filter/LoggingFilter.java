package com.axamit.core.filter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

@Component(service = Filter.class,
        property = {Constants.SERVICE_DESCRIPTION + "=Filter for logging request",
                EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
                Constants.SERVICE_RANKING + "=-700"
        })
public class LoggingFilter extends BaseSlingHttpFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        SlingHttpServletRequest servletRequest = asSlingRequest(request);
        String resourcePath = servletRequest.getRequestPathInfo().getResourcePath();
        if (resourcePath.toLowerCase().contains("axamit")) {
            String format = String.format("Executing request on path: %s", resourcePath);
            LOGGER.debug(format);
        }
        super.doFilter(request, response, filterChain);
    }
}