package com.axamit.core.filter.verification;

import com.axamit.core.util.FileExtension;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;

import java.util.stream.Stream;

public class SlingUrlVerifier extends BaseSlingVerifier<SlingHttpServletRequest> {
    private String correctPath;

    public SlingUrlVerifier(String correctPath) {
        this.correctPath = correctPath;
    }

    @Override
    public boolean isVerified(SlingHttpServletRequest request) {
        RequestPathInfo requestPathInfo = request.getRequestPathInfo();
        String path = requestPathInfo.getResourcePath();
        if (!path.contains(correctPath)) {
            return false;
        }
        int beginIndex = path.lastIndexOf('.');
        if (beginIndex < correctPath.length()) {
            return false;
        }
        String extension = path.substring(beginIndex + 1);
        boolean isValidExtension = !extension.isEmpty() && Stream.of(FileExtension.values())
                .anyMatch(fileExtension -> extension.equalsIgnoreCase(fileExtension.toString()));
        if (!isValidExtension) {
            return false;
        }
        String selector = requestPathInfo.getSelectorString();
        if (!(selector == null || selector.trim().isEmpty())) {
            return false;
        }
        return verifyNext(request);
    }
}