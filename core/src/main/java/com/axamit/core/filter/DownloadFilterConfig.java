package com.axamit.core.filter;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.AttributeType;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Download filter config")
public @interface DownloadFilterConfig {

    @AttributeDefinition(name = "File downloading path", type = AttributeType.STRING)
    String getFolderPath() default "/content/dam/projects/axamit-com/";

    @AttributeDefinition(name = "Group", type = AttributeType.STRING)
    String getGroupName() default "everyone";

    @AttributeDefinition(name = "Redirect Path", type = AttributeType.STRING)
    String getRedirectPath() default "https://www.axamit.com/";
}