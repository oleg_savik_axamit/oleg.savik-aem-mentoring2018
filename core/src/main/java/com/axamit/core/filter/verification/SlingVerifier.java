package com.axamit.core.filter.verification;

public interface SlingVerifier<T> {
    SlingVerifier<T> setNext(SlingVerifier<T> slingVerifier);

    boolean isVerified(T request);
}