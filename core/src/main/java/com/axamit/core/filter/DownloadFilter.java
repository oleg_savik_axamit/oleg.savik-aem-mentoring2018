package com.axamit.core.filter;

import com.axamit.core.filter.verification.SlingUrlVerifier;
import com.axamit.core.filter.verification.UserGroupVerifier;
import com.axamit.core.service.upload.FileSystemUploadService;
import com.axamit.core.service.upload.UploadService;
import com.axamit.core.util.AssetUtils;
import com.day.cq.dam.api.Asset;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.EngineConstants;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;
import java.util.Optional;

@Designate(ocd = DownloadFilterConfig.class)
@Component(service = Filter.class, immediate = true,
        property = {Constants.SERVICE_DESCRIPTION + "=Filter for downloading asset from specified jcr folder",
                EngineConstants.SLING_FILTER_SCOPE + "=" + EngineConstants.FILTER_SCOPE_REQUEST,
                Constants.SERVICE_RANKING + "=-700"
        })
public class DownloadFilter extends BaseSlingHttpFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadFilter.class);
    private String redirectPath;
    private SlingUrlVerifier urlVerifier;
    private UserGroupVerifier userGroupVerifier;
    @Reference(service = FileSystemUploadService.class)
    private UploadService<Asset> uploadService;

    @Activate
    protected void activate(final DownloadFilterConfig config) {
        redirectPath = config.getRedirectPath();
        urlVerifier = new SlingUrlVerifier(config.getFolderPath());
        userGroupVerifier = new UserGroupVerifier(config.getGroupName());
        urlVerifier.setNext(userGroupVerifier);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        SlingHttpServletRequest slingRequest = asSlingRequest(servletRequest);
        SlingHttpServletResponse slingResponse = asSlingResponse(servletResponse);
        if (urlVerifier.isVerified(slingRequest)) {
            uploadService = new FileSystemUploadService(slingResponse, userGroupVerifier.getUserId());
            String assetPath = slingRequest.getRequestPathInfo().getResourcePath();
            AssetUtils.resolveAsset(assetPath, slingRequest.getResourceResolver())
                    .ifPresent(uploadService::upload);
        } else {
            Optional.ofNullable(userGroupVerifier.isUserGroupCorrect()).ifPresent(aBoolean -> sendRedirect(slingResponse, redirectPath));
        }
        super.doFilter(servletRequest, servletResponse, filterChain);
    }

    private void sendRedirect(SlingHttpServletResponse response, String redirectPath) {
        try {
            response.sendRedirect(redirectPath);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}