package com.axamit.core.filter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import javax.servlet.*;
import java.io.IOException;

public abstract class BaseSlingHttpFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }

    SlingHttpServletRequest asSlingRequest(ServletRequest request) {
        return (SlingHttpServletRequest) request;
    }

    SlingHttpServletResponse asSlingResponse(ServletResponse response) {
        return (SlingHttpServletResponse) response;
    }
}