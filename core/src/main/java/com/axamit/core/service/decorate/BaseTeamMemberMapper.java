package com.axamit.core.service.decorate;

import org.apache.sling.api.resource.ValueMap;

public abstract class BaseTeamMemberMapper<T> implements TeamMemberMapper<T> {
    private static final long serialVersionUID = 3206274086355867497L;
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String IMAGE_PATH = "imagePath";
    private static final String ROLE = "role";
    private static final String DELIMITER = " ";

    public String getFullName(ValueMap map) {
        String firstName = map.get(FIRST_NAME, String.class);
        String lastName = map.get(LAST_NAME, String.class);
        return String.join(DELIMITER, firstName, lastName);
    }

    public boolean check(ValueMap map) {
        return map.containsKey(FIRST_NAME) &&
                map.containsKey(LAST_NAME) &&
                map.containsKey(ROLE) &&
                map.containsKey(IMAGE_PATH);
    }
}