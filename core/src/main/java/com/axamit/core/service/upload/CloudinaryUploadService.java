package com.axamit.core.service.upload;

import com.axamit.core.util.AssetUtils;
import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import com.day.cq.dam.api.Asset;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component(service = CloudinaryUploadService.class)
public class CloudinaryUploadService implements UploadService<Asset> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CloudinaryUploadService.class);
    private static final Map<String, String> CLOUD_CREDENTIALS;

    static {
        Map<String, String> options = new HashMap<>();
        options.put("cloud_name", "dednwsa5u");
        options.put("api_key", "736834885834682");
        options.put("api_secret", "0xCM7_O_-mLWoqEyZNN3g_1cco0");
        CLOUD_CREDENTIALS = Collections.unmodifiableMap(options);
    }

    @Override
    public void upload(Asset asset) {
        byte[] bytes = AssetUtils.toBytes(asset);
        Cloudinary cloudinary = new Cloudinary();
        Uploader uploader = cloudinary.uploader();
        try {
            uploader.upload(bytes, CLOUD_CREDENTIALS);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}