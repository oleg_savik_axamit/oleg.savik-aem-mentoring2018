package com.axamit.core.service.upload;

import com.axamit.core.util.AssetUtils;
import com.day.cq.dam.api.Asset;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

@Component(service = FileSystemUploadService.class)
public class FileSystemUploadService implements UploadService<Asset> {
    private static final String CONTENT_DISPOSITION = "Content-Disposition";
    private static final String FILENAME = "attachment;filename=";
    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemUploadService.class);
    private SlingHttpServletResponse slingResponse;
    private String userId;

    public FileSystemUploadService(SlingHttpServletResponse slingResponse, String userId) {
        this.slingResponse = slingResponse;
        this.userId = userId;
    }

    @Override
    public void upload(Asset asset) {
        String fileName = FILENAME + userId + '_' + asset.getName();
        byte[] bytes = AssetUtils.toBytes(asset);
        try (ServletOutputStream stream = slingResponse.getOutputStream()) {
            slingResponse.setHeader(CONTENT_DISPOSITION, fileName);
            stream.write(bytes);
            stream.flush();
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }
}