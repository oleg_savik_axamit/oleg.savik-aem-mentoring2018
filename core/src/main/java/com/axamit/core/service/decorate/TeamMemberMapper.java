package com.axamit.core.service.decorate;

import org.apache.sling.api.resource.Resource;

import java.io.Serializable;

public interface TeamMemberMapper<T> extends Serializable {
    T map(Resource resource);
}