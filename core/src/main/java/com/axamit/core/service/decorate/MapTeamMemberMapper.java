package com.axamit.core.service.decorate;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.service.component.annotations.Component;

import java.util.Collections;
import java.util.Map;

@Component(service = MapTeamMemberMapper.class)
public class MapTeamMemberMapper extends BaseTeamMemberMapper<Map<String, String>> {

    private static final long serialVersionUID = 6595082729139234953L;

    @Override
    public Map<String, String> map(Resource memberResource) {
        ValueMap valueMap = memberResource.getValueMap();
        if (!check(valueMap)) {
            return Collections.emptyMap();
        }
        String fullName = getFullName(valueMap);
        String path = memberResource.getPath();
        return Collections.singletonMap(fullName, path);
    }
}