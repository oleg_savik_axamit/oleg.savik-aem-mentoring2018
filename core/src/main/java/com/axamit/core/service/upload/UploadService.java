package com.axamit.core.service.upload;

public interface UploadService<T> {
    void upload(T item);
}