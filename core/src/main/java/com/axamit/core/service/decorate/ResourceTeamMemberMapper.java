package com.axamit.core.service.decorate;

import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.osgi.service.component.annotations.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component(service = ResourceTeamMemberMapper.class)
public class ResourceTeamMemberMapper extends BaseTeamMemberMapper<Resource> {
    private static final long serialVersionUID = -6872962723965320169L;
    private static final String TEXT = "text";
    private static final String VALUE = "value";

    @Override
    public Resource map(Resource memberResource) {
        Map<String, Object> map = wrapToMap(memberResource);
        return new ValueMapResource(memberResource.getResourceResolver(),
                new ResourceMetadata(),
                JcrConstants.NT_UNSTRUCTURED,
                new ValueMapDecorator(Collections.unmodifiableMap(map)));
    }

    private Map<String, Object> wrapToMap(Resource memberResource) {
        ValueMap valueMap = memberResource.getValueMap();
        if (check(valueMap)) {
            return Collections.emptyMap();
        }
        Map<String, Object> map = new HashMap<>();
        map.put(VALUE, memberResource.getPath());
        map.put(TEXT, getFullName(valueMap));
        return map;
    }
}