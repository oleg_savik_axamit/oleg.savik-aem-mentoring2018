package com.axamit.core.model.content.item;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContactItem implements ContentItem {
    @Inject
    private String title;
    @Inject
    private String phoneNumber;
    @Inject
    private String email;
    @Inject
    private String skype;
}