package com.axamit.core.model.content;

import com.axamit.core.model.content.item.TeamMemberItem;
import com.axamit.core.util.ItemUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TeamServletModel {
    private static final String DYNAMIC = "dynamic";
    @Inject
    private static ResourceResolver resolver;
    @Inject
    private Resource teamMembers;
    @Getter
    private List<TeamMemberItem> members;
    @Inject
    @Getter
    private String title;
    @Inject
    @Getter
    private String description;

    @PostConstruct
    private void init() {
        Function<Resource, TeamMemberItem> teamMemberFunction = resource -> {
            String resourcePath = (String) resource.getValueMap().get(DYNAMIC);
            Resource resolvedResource = resolver.resolve(resourcePath);
            return resolvedResource.adaptTo(TeamMemberItem.class);
        };
        members = ItemUtils.nonAdaptableItems(teamMembers, teamMemberFunction)
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }
}