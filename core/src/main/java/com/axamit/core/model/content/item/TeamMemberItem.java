package com.axamit.core.model.content.item;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TeamMemberItem implements ContentItem {
    @Inject
    private String firstName;
    @Inject
    private String lastName;
    @Inject
    private String role;
    @Inject
    private String imagePath;
}