package com.axamit.core.model.content;

import com.axamit.core.model.content.item.ContentItem;
import com.axamit.core.util.ItemUtils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public abstract class BaseListModel<T extends ContentItem> {
    @Getter
    private Collection<T> items;

    public void init(Resource itemResource, Class<T> tClass) {
        items = ItemUtils.adaptableItems(itemResource, tClass)
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }
}