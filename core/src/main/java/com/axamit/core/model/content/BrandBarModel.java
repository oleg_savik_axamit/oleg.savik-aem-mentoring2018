package com.axamit.core.model.content;

import com.axamit.core.model.content.item.BrandBarItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BrandBarModel extends BaseListModel<BrandBarItem> {
    @Inject
    private Resource brandItems;

    @PostConstruct
    private void init() {
        super.init(brandItems, BrandBarItem.class);
    }
}