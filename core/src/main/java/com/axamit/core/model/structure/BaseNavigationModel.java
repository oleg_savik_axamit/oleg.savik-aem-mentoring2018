package com.axamit.core.model.structure;

import com.adobe.granite.ui.components.ds.ValueMapResource;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.wrappers.ValueMapDecorator;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.day.cq.wcm.api.NameConstants.NT_PAGE;

public abstract class BaseNavigationModel {
    private static final String LINK_URL = "linkUrl";
    private static final String LINK_TEXT_KEY = "linkText";
    private static final String INVALID_LINK = "INVALID LINK";
    private static final String ERROR_PATH = "/libs/sling/servlet/errorhandler/404.jsp";
    @Inject
    private ResourceResolver resolver;
    @Getter
    private List<Resource> items;

    public void init(Resource itemResource) {
        items = getChildren(itemResource);
    }

    private List<Resource> getChildren(Resource pagesResource) {
        if (pagesResource == null || !pagesResource.hasChildren()) {
            return Collections.emptyList();
        }
        return StreamSupport.stream(pagesResource.getChildren().spliterator(), false)
                .map(Resource::getValueMap)
                .map(this::toResource)
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }

    private Resource toResource(ValueMap map) {
        return Optional.of(map)
                .map(valueMap -> {
                    String s = (String) valueMap.get(LINK_URL);
                    return resolver.resolve(s);
                })
                .filter(resource -> NT_PAGE.equals(resource.getResourceType()))
                .map(resource -> {
                    String path = resource.getPath();
                    String linkText = Optional.ofNullable(map.get(LINK_TEXT_KEY, String.class)).orElseGet(resource::getName);
                    List<Resource> pageChildren = getPageChildren(resource);
                    return newPageResource(path, linkText, pageChildren);
                })
                .orElseGet(() -> newPageResource(ERROR_PATH, INVALID_LINK, Collections.emptyList()));
    }

    private Resource newPageResource(String linkPath, String linkText, Collection<Resource> children) {
        return new ValueMapResource(resolver, linkPath, NT_PAGE, new ValueMapDecorator(Collections.singletonMap(LINK_TEXT_KEY, linkText)), children);
    }

    private List<Resource> getPageChildren(Resource resource) {
        return StreamSupport.stream(resource.getChildren().spliterator(), false)
                .filter(resource1 -> NT_PAGE.equals(resource1.getResourceType()))
                .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList));
    }
}