package com.axamit.core.model.content.item;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CarouselItem implements ContentItem {
    @Inject
    private String slideTitle;
    @Inject
    private String slideDescription;
    @Inject
    private String slideLinkUrl;
    @Inject
    private String slideLinkText;
    @Inject
    private String slideImage;
    @Inject
    private String slideSmallImage;
}