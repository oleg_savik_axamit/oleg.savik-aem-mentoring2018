package com.axamit.core.model.content;

import com.axamit.core.model.content.item.TeamMemberItem;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TeamModel extends BaseListModel<TeamMemberItem> {
    @Inject
    @Getter(AccessLevel.NONE)
    private Resource teamMembers;

    @Inject
    private String title;

    @Inject
    private String description;

    @PostConstruct
    private void init() {
        super.init(teamMembers, TeamMemberItem.class);
    }
}