package com.axamit.core.model.content;

import com.axamit.core.model.content.item.CarouselItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CarouselModel extends BaseListModel<CarouselItem> {
    @Inject
    private Resource slides;

    @PostConstruct
    private void init() {
        super.init(slides, CarouselItem.class);
    }
}