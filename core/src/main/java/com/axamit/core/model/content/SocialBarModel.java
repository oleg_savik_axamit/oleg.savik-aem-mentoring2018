package com.axamit.core.model.content;

import com.axamit.core.model.content.item.SocialItem;
import lombok.AccessLevel;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialBarModel extends BaseListModel<SocialItem> {
    @Inject
    @Getter(AccessLevel.NONE)
    private Resource socialItems;

    @Inject
    private String printIcon;

    @Inject
    private String writeIcon;

    @PostConstruct
    private void init() {
        super.init(socialItems, SocialItem.class);
    }
}