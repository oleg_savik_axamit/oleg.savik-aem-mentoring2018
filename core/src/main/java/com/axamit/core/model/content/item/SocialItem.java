package com.axamit.core.model.content.item;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import javax.inject.Named;

@AllArgsConstructor
@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialItem implements ContentItem {
    @Inject
    @Named("itemIcon")
    private String iconPath;
    @Inject
    @Named("itemLink")
    private String link;
}