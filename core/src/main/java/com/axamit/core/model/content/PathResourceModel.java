package com.axamit.core.model.content;

import org.apache.commons.lang.time.StopWatch;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static com.day.cq.wcm.api.NameConstants.NT_PAGE;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PathResourceModel {
    @Inject
    private String resourceLink;
    @Inject
    private ResourceResolver resourceResolver;
    private Collection<Resource> collection;
    private Long initTime;

    @PostConstruct
    private void init() {
        StopWatch watch = new StopWatch();
        watch.start();
        Resource resource = resourceResolver.resolve(resourceLink);
        collection = deepFirstCollectionInit(resource, new ArrayList<>());
        watch.stop();
        initTime = watch.getTime();
    }

    private Collection<Resource> deepFirstCollectionInit(Resource resource, Collection<Resource> list) {
        if (resource.hasChildren()) {
            if (NT_PAGE.equals(resource.getResourceType())) {
                list.add(resource);
            }
            for (Resource children : resource.getChildren()) {
                deepFirstCollectionInit(children, list);
            }
        }
        return list;
    }

    public Long getInitTime() {
        return initTime;
    }

    public Collection<Resource> getCollection() {
        return Collections.unmodifiableCollection(collection);
    }
}