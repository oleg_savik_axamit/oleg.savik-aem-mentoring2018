package com.axamit.core.model.content;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Getter
public class TweetModel {
    @Inject
    private String tweetTitle;
    @Inject
    private String tweetDescription;
    @Inject
    private String tweetLink;
    @Inject
    private int tweetLimit;
}