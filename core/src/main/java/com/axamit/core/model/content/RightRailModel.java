package com.axamit.core.model.content;

import com.axamit.core.model.content.item.ContactItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RightRailModel extends BaseListModel<ContactItem> {
    @Inject
    private Resource contactItems;

    @PostConstruct
    private void init() {
        super.init(contactItems, ContactItem.class);
    }
}