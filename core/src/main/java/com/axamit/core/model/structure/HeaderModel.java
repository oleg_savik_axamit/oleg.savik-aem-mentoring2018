package com.axamit.core.model.structure;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderModel extends BaseNavigationModel {
    @Inject
    private Resource items;

    @Inject
    @Getter
    private String file;

    @PostConstruct
    private void init() {
        super.init(items);
    }
}