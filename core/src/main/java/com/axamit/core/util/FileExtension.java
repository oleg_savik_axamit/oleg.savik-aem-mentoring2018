package com.axamit.core.util;

public enum FileExtension {
    PDF, DOC, DOCX;

    @Override
    public String toString() {
        return this.name();
    }
}