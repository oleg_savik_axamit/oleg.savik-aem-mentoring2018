package com.axamit.core.util;

import com.axamit.core.model.content.item.ContentItem;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public final class ItemUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemUtils.class);

    private ItemUtils() {
        throw new UnsupportedOperationException();
    }

    public static <K extends ContentItem> Stream<K> adaptableItems(Resource resource, Class<K> tClass) {
        Stream<K> stream;
        try {
            Class<K> kClass = Optional.ofNullable(tClass)
                    .filter(ItemUtils::isAdapteble)
                    .orElseThrow(IllegalArgumentException::new);
            stream = childItems(resource, resource1 -> resource1.adaptTo(kClass));
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
            stream = Stream.empty();
        }
        return stream;
    }

    public static <T> Stream<T> nonAdaptableItems(Resource resource, Function<Resource, T> function) {
        Stream<T> stream;
        try {
            Function<Resource, T> validFunction = Optional.ofNullable(function)
                    .orElseThrow(IllegalArgumentException::new);
            stream = childItems(resource, validFunction);
        } catch (IllegalArgumentException e) {
            LOGGER.error(e.getMessage());
            stream = Stream.empty();
        }
        return stream;
    }

    private static <T> Stream<T> childItems(Resource resource, Function<Resource, T> function) {
        return Optional.ofNullable(resource)
                .map(resource1 -> StreamSupport.stream(resource1.getChildren().spliterator(), false).map(function))
                .orElseThrow(IllegalArgumentException::new);
    }

    private static <K> boolean isAdapteble(Class<K> tClass) {
        if (tClass == null) {
            return false;
        }
        Model annotation = tClass.getAnnotation(Model.class);
        Class<?>[] classes = annotation.adaptables();
        for (Class<?> adaptable : classes) {
            if (adaptable.equals(Resource.class)) {
                return true;
            }
        }
        return false;
    }
}