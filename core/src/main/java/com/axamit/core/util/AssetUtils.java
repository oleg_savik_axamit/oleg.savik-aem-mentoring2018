package com.axamit.core.util;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.Rendition;
import org.apache.commons.io.IOUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public final class AssetUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssetUtils.class);
    private static final byte[] EMPTY_BYTES = new byte[0];

    private AssetUtils() {
        throw new UnsupportedOperationException();
    }

    public static Optional<Asset> resolveAsset(String path, ResourceResolver resourceResolver) {
        return Optional.ofNullable(resourceResolver)
                .map(resolver -> resolver.resolve(path))
                .flatMap(resource -> Optional.ofNullable(resource.adaptTo(Asset.class)));
    }

    public static byte[] toBytes(Asset asset) {
        return Optional.ofNullable(asset)
                .map(Asset::getOriginal)
                .map(Rendition::getStream)
                .map(AssetUtils::getBytes)
                .orElse(EMPTY_BYTES);
    }

    private static byte[] getBytes(InputStream stream) {
        byte[] assetBytes;
        try {
            assetBytes = IOUtils.toByteArray(stream);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            assetBytes = EMPTY_BYTES;
        }
        return assetBytes;
    }
}