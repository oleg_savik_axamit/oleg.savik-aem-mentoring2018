package com.axamit.core.scheduler;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Scheduled task",
        description = "Simple demo for cron-job like task with properties")
public @interface ScheduledTaskConfig {
    @AttributeDefinition(name = "Cron-job expression")
    String scheduler_expression() default "*/5 * * * * ?";

    @AttributeDefinition(name = "Concurrent task",
            description = "Whether or not to schedule this task concurrently")
    boolean scheduler_concurrent() default false;

    @AttributeDefinition(name = "A parameter",
            description = "Can be configured in /system/console/configMgr")
    String myParameter() default "default value";
}