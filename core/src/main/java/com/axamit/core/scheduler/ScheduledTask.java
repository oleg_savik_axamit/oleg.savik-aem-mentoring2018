package com.axamit.core.scheduler;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A simple demo for cron-job like tasks that get executed regularly.
 * It also demonstrates how property values can be set. Users can
 * set the property values in /system/console/configMgr
 */
@Designate(ocd = ScheduledTaskConfig.class)
/*@Component(service = Runnable.class)*/
public class ScheduledTask implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTask.class);
    private String myParameter;

    @Override
    public void run() {
        LOGGER.info("ScheduledTask is now running, myParameter='{}'", myParameter);
    }

    @Activate
    protected void activate(final ScheduledTaskConfig config) {
        myParameter = config.myParameter();
    }
}