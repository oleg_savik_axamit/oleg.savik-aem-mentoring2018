package com.axamit.core.servlet;

import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.axamit.core.service.decorate.ResourceTeamMemberMapper;
import com.axamit.core.service.decorate.TeamMemberMapper;
import com.axamit.core.util.ItemUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component(service = Servlet.class, immediate = true, property = {
        Constants.SERVICE_DESCRIPTION + "= Team data source servlet",
        "sling.servlet.methods =" + HttpConstants.METHOD_GET,
        "sling.servlet.paths =" + "/bin/axamit/team_datasource"}
)
public class TeamDataSourceServlet extends SlingSafeMethodsServlet {
    private static final String DATA_SOURCE_URL = "/content/axamit/admin_page/jcr:content/parcontent/teamlist/teamMembers";
    private static final String DATA_SOURCE_ATTRIBUTE_NAME = "dataSource";
    private static final long serialVersionUID = -6305924859968359318L;

    @Reference(service = ResourceTeamMemberMapper.class)
    private TeamMemberMapper<Resource> decorator;

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response) {
        Resource resource = request.getResourceResolver().resolve(DATA_SOURCE_URL);
        Optional.of(resource)
                .map(r -> ItemUtils.nonAdaptableItems(r, decorator::map))
                .filter(resourceStream -> !Stream.empty().equals(resourceStream))
                .map(stream -> stream.collect(Collectors.toList()))
                .ifPresent(resourceList -> writeToRequest(request, resourceList));
    }

    private void writeToRequest(SlingHttpServletRequest request, List<Resource> sourceList) {
        SimpleDataSource dataSource = new SimpleDataSource(sourceList.iterator());
        request.setAttribute(DATA_SOURCE_ATTRIBUTE_NAME, dataSource);
    }
}