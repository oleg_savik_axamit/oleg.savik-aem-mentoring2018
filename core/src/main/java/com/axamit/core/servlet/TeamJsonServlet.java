package com.axamit.core.servlet;

import com.axamit.core.service.decorate.MapTeamMemberMapper;
import com.axamit.core.service.decorate.TeamMemberMapper;
import com.axamit.core.util.ItemUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.servlet.Servlet;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@Component(immediate = true, service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "= Team servlet ,which sends json response",
        "sling.servlet.extension=json",
        "sling.servlet.paths=/bin/axamit/teamMembers",
        "sling.servlet.methods=get"})
public class TeamJsonServlet extends SlingSafeMethodsServlet {
    private static final String DATA_SOURCE_URL = "/content/axamit/admin_page/jcr:content/parcontent/teamlist/teamMembers";
    private static final String JSON_TYPE = "application/json";
    private static final Logger LOGGER = LoggerFactory.getLogger(TeamJsonServlet.class);
    private static final long serialVersionUID = 4630243468609610399L;

    @Reference(service = MapTeamMemberMapper.class)
    private TeamMemberMapper<Map<String, String>> decorator;

    @Override
    protected void doGet(@Nonnull SlingHttpServletRequest request, @Nonnull SlingHttpServletResponse response) {
        Resource resource = request.getResourceResolver().resolve(DATA_SOURCE_URL);
        Optional.of(resource)
                .map(resource1 -> ItemUtils.nonAdaptableItems(resource1, decorator::map).collect(JsonCollector.toJson()))
                .filter(StringUtils::isNotEmpty)
                .ifPresent(s -> writeToResponse(response, s));

    }

    private void writeToResponse(SlingHttpServletResponse response, String sourceString) {
        try {
            response.getWriter().write(sourceString);
            response.setContentType(JSON_TYPE);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}