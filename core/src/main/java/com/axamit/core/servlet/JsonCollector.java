package com.axamit.core.servlet;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public final class JsonCollector<T> implements Collector<T, JsonCollector<T>, String> {
    private List<T> list;

    private JsonCollector() {
        list = new ArrayList<>();
    }

    public static <T> JsonCollector<T> toJson() {
        return new JsonCollector<>();
    }

    @Override
    public Supplier<JsonCollector<T>> supplier() {
        return JsonCollector::new;
    }

    @Override
    public BiConsumer<JsonCollector<T>, T> accumulator() {
        return JsonCollector::add;
    }

    @Override
    public BinaryOperator<JsonCollector<T>> combiner() {
        return JsonCollector::merge;
    }

    @Override
    public Function<JsonCollector<T>, String> finisher() {
        return JsonCollector::build;
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.emptySet();
    }

    private void add(T item) {
        list.add(item);
    }

    private JsonCollector<T> merge(JsonCollector<T> collector) {
        this.list.addAll(collector.list);
        return this;
    }

    private String build() {
        return new Gson().toJson(list);
    }
}