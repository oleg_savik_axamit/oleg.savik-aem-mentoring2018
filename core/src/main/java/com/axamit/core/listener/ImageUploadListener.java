package com.axamit.core.listener;

import com.axamit.core.service.upload.CloudinaryUploadService;
import com.axamit.core.service.upload.UploadService;
import com.axamit.core.util.AssetUtils;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component(
        service = EventHandler.class,
        immediate = true,
        property = {
                Constants.SERVICE_DESCRIPTION + "=listen uploading",
                EventConstants.EVENT_FILTER + "(path=/content/dam/projects/axamit-com/*)",
                EventConstants.EVENT_FILTER + "(path=/content/dam/projects/axamit-com/*)",
                EventConstants.EVENT_TOPIC + "=org/apache/sling/api/resource/Resource/ADDED"
        }
)
public class ImageUploadListener implements EventHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ImageUploadListener.class);
    private static final Map<String, Object> ADMIN_RESOLVER_CREDENTIALS;
    private static final String ADMIN = "admin";
    @Reference
    private ResourceResolverFactory resolverFactory;
    @Reference(service = CloudinaryUploadService.class)
    private UploadService<Asset> uploadService;

    static {
        Map<String, Object> map = new HashMap<>();
        map.put(ResourceResolverFactory.USER, ADMIN);
        map.put(ResourceResolverFactory.PASSWORD, ADMIN.toCharArray());
        ADMIN_RESOLVER_CREDENTIALS = Collections.unmodifiableMap(map);
    }

    @Override
    public void handleEvent(Event event) {
        String type = String.valueOf(event.getProperty(SlingConstants.PROPERTY_RESOURCE_TYPE));
        if (DamConstants.NT_DAM_ASSET.equals(type)) {
            String assetPath = (String) event.getProperty(SlingConstants.PROPERTY_PATH);
            AssetUtils.resolveAsset(assetPath, getAdminResourceResolver()).ifPresent(uploadService::upload);
        }
    }

    private ResourceResolver getAdminResourceResolver() {
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resolverFactory.getResourceResolver(ADMIN_RESOLVER_CREDENTIALS);
        } catch (LoginException e) {
            LOGGER.error(e.getMessage());
        }
        return resourceResolver;
    }
}